﻿using System;
using System.Collections.Generic;
using System.Web.Http.Cors;
using System.Web.Mvc;
using Geo.DAL;
using RestSharp;
using Parameter = Geo.DAL.Parameter;

// TODO: Запрос списка проектов
// TODO: Рендеринг в меню список проектов
// TODO: Дата загрузки
// TODO: Рендеринг параметров
// TODO:
// TODO:
// TODO:
// TODO: Запрос проекта

namespace Geo.Frontend.Controllers
{
    [EnableCors("*", "*", "*")]
    public class HomeController : Controller
    {
        private string _backendBaseUrl = "http://localhost:12341/";

        public ActionResult Index()
        {
            return View();
        }
    }

    public class ProjectsViewModel
    {
        public Project[] Projects { get; set; }
        public ProjectsViewModel(List<Project> projects)
        {
            Projects = projects.ToArray();
        }
    }
}