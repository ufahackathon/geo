﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geo.DAL;

namespace Geo.Test
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var initializer = new NHibernateinitializer();
                var unitOfWorkFactory =
                    new NHibernateUnitOfWorkFactory(initializer.GetConfiguration().BuildSessionFactory());
                var repositoryFactory = new RepositoryFactory();

                using (var uow = unitOfWorkFactory.Create())
                {
                    var rep = repositoryFactory.Create<int, Project>(uow.Session);
                    var all = rep.All();
                }

            }
            catch (Exception e)
            {
            }
        }
    }
}
