﻿using System;
using System.Linq;
using NHibernate;
using NHibernate.Linq;

namespace Geo.DAL
{
    public abstract class NHibernateRepositoryBase<TId, TEntity> : IRepository<TId, TEntity>
        where TEntity : class, ICommonEntity<TId>

    {
        private readonly ISession _session;

        /// <summary>
        /// </summary>
        /// <param name="session"></param>
        /// <exception cref="ArgumentNullException">
        ///   <c>sessionProvider</c>
        ///   is null.</exception>
        protected NHibernateRepositoryBase( ISession session)
        {
            if (session == null)
                throw new ArgumentNullException("session");

            _session = session;
        }

        protected ISession Session
        {
            get { return _session; }
        }

        #region IRepository<TEntity> Members

        public virtual IQueryable<TEntity> All()
        {
            return Session.Query<TEntity>();
        }

        public virtual TEntity Get(TId id)
        {
            return Session.Get<TEntity>(id);
        }

        public virtual void Add(TEntity entity)
        {
            Session.Save(entity);
        }

        public virtual void Update(TEntity entity)
        {
            Session.Update(entity);
        }

        public virtual void Remove(TEntity entity)
        {
            Session.Delete(entity);
        }

        #endregion
    }
}