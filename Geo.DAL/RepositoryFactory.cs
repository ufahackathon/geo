﻿using NHibernate;

namespace Geo.DAL
{
    public class RepositoryFactory : IRepositoryFactory
    {
        public IRepository<TId, TEntity> Create<TId, TEntity>(ISession session) where TEntity : class, ICommonEntity<TId>
        {
            return new NHibernateRepository<TId, TEntity>(session);
        }
    }
}