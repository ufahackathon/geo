﻿using NHibernate;

namespace Geo.DAL
{
    public interface IRepositoryFactory
    {
        IRepository<TId, TEntity> Create<TId, TEntity>(ISession session) where TEntity : class, ICommonEntity<TId>;
    }
}