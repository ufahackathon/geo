using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Geo.DAL;


namespace Geo.DAL {
    
    public class Project : ICommonEntity<int>
    {
        public Project() {}

        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual ISet<Bundle> Bundles { get; set; }
    }
}
