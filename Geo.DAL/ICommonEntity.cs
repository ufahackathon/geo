﻿namespace Geo.DAL
{
    public interface ICommonEntity<TId>
    {
        TId Id { get; }
    }
}
