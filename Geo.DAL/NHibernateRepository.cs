﻿using NHibernate;

namespace Geo.DAL
{
    public sealed class NHibernateRepository<TId, TEntity> : NHibernateRepositoryBase<TId, TEntity>
        where TEntity : class, ICommonEntity<TId>
    {
        /// <summary>
        ///   Конструктор
        /// </summary>
        /// <param name="session"> </param>
        public NHibernateRepository(ISession session)
            : base(session)
        {
        }
    }
}