﻿using NHibernate;

namespace Geo.DAL
{
    public interface ISessionProvider
    {
        ISession CurrentSession { get; }
    }
}