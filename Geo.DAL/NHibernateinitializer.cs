﻿using NHibernate.Cfg;

namespace Geo.DAL
{
    public class NHibernateinitializer
    {
        public Configuration GetConfiguration()
        {
            var config = new Configuration();
            config.Configure();
            return config;
        }
    }
}