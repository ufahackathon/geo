using System.ComponentModel.DataAnnotations;

namespace Geo.DAL {
    
    public class Data
    {
        public virtual string Geo1 { get; set; }
        public virtual string Geo2 { get; set; }
        public virtual string Value { get; set; }
        public virtual int ParamId { get; set; }

        protected bool Equals(Data other)
        {
            return string.Equals(Geo1, other.Geo1) && string.Equals(Geo2, other.Geo2) && string.Equals(Value, other.Value) && ParamId == other.ParamId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Data) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int hashCode = (Geo1 != null ? Geo1.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Geo2 != null ? Geo2.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ (Value != null ? Value.GetHashCode() : 0);
                hashCode = (hashCode*397) ^ ParamId;
                return hashCode;
            }
        }
    }
}
