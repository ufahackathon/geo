﻿using System;
using NHibernate;

namespace Geo.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        ISession Session { get; }

        /// <summary>
        ///     Сохранить ВСЕ изменения в базу
        /// </summary>
        void Commit();

        /// <summary>
        ///     Пометить сущность для сохранения в базу
        /// </summary>
        void Save<TId, TEntity>(TEntity entity)
            where TEntity : class, ICommonEntity<TId>, new();

        /// <summary>
        ///     Пометить сущность для удаления из базы
        /// </summary>
        void Delete<TId, TEntity>(TEntity entity)
            where TEntity : class, ICommonEntity<TId>, new();
    }
}