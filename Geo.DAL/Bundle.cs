using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Geo.DAL {
    
    public class Bundle {
        public virtual int Id { get; set; }
        public virtual DateTime Datetime { get; set; }
        public virtual ISet<Parameter> Parameters { get; set; }
    }
}
