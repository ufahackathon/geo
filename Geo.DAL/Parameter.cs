namespace Geo.DAL {
    
    public class Parameter {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Type { get; set; }
    }
}
