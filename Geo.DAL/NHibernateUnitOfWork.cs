﻿using System;
using System.Data;
using NHibernate;
using NHibernate.Context;

namespace Geo.DAL
{
    internal class NHibernateUnitOfWork : IUnitOfWork
    {
        public ISession Session { get; private set; }
        private ITransaction _transaction;

        public NHibernateUnitOfWork(ISession session, IsolationLevel isolationLevel = IsolationLevel.ReadCommitted)
        {
            if (session == null)
                throw new ArgumentNullException("session");

            CurrentSessionContext.Bind(session);

            Session = session;
            _transaction = session.BeginTransaction(isolationLevel);
        }

        #region IUnitOfWork Members

        public void Dispose()
        {
            if (!_transaction.WasCommitted && !_transaction.WasRolledBack)
                _transaction.Rollback();
            _transaction.Dispose();
            _transaction = null;

            CurrentSessionContext.Unbind(Session.SessionFactory);
            Session.Dispose();
        }

        public void Commit()
        {
            _transaction.Commit();
        }

        public void Save<TId, TEntity>(TEntity entity)
            where TEntity : class, ICommonEntity<TId>, new()
        {
            Session.Save(entity);
        }

        public void Delete<TId, TEntity>(TEntity entity)
            where TEntity : class, ICommonEntity<TId>, new()
        {
            Session.Delete(entity);
        }

        #endregion
    }
}