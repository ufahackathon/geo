﻿using System.Linq;

namespace Geo.DAL
{
    /// <summary>
    ///     Интерфейс репозитория
    /// </summary>
    /// <typeparam name="TEntity"> Тип сущности доменной модели </typeparam>
    /// <typeparam name="TId">Тип ключевого столбца</typeparam>
    public interface IRepository<TId, TEntity>
        where TEntity : ICommonEntity<TId>
    {
        /// <summary>
        ///     Получить все сущности
        /// </summary>
        /// <returns> Список сущностей </returns>
        IQueryable<TEntity> All();

        /// <summary>
        ///     Получить сущность по идентификатору. В ряде случаев использование Load более предпочтительно.
        ///     Подробнее http://ayende.com/Blog/archive/2009/04/30/nhibernate-ndash-the-difference-between-get-load-and-querying-by.aspx
        /// </summary>
        /// <param name="id"> Идектификатор сущности </param>
        /// <returns> Сущность с указанным Id, если существует. Иначе - null. </returns>
        TEntity Get(TId id);

        /// <summary>
        ///     Сохранить сущность
        /// </summary>
        /// <param name="entity"> Сущность </param>
        void Add(TEntity entity);

        /// <summary>
        /// Обновление сущности
        /// </summary>
        /// <param name="entity"> Сущность </param>
        void Update(TEntity entity);

        /// <summary>
        ///     Удалить сущность
        /// </summary>
        /// <param name="entity"> Сущность </param>
        void Remove(TEntity entity);
    }
}