﻿using System;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using Geo.DAL;
using NHibernate.Linq;

namespace Geo.Backend.Controllers
{
    [EnableCors("*", "*", "*")]
    public class DataController : ApiController
    {
        private readonly NHibernateUnitOfWorkFactory _unitOfWorkFactory;
        private readonly RepositoryFactory _repositoryFactory;

        public DataController()
        {
            _unitOfWorkFactory = new NHibernateUnitOfWorkFactory((new NHibernateinitializer()).GetConfiguration().BuildSessionFactory());
            _repositoryFactory = new RepositoryFactory();
        }

        public Project[] GetProjects()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var projectRepository = _repositoryFactory.Create<int, Project>(uow.Session);
                return projectRepository.All().ToArray();
            }
        }

        public Data[] GetObjects()
        {
            using (var uow = _unitOfWorkFactory.Create())
            {
                var data = uow
                    .Session
                    .Query<Data>()
                    .ToArray();

                return data;
            }
        }
    }
}
