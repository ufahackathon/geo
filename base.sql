-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.5.28 - MySQL Community Server (GPL)
-- Server OS:                    Win32
-- HeidiSQL version:             7.0.0.4053
-- Date/time:                    2014-11-22 01:08:06
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET FOREIGN_KEY_CHECKS=0 */;

-- Dumping database structure for geo
DROP DATABASE IF EXISTS `geo`;
CREATE DATABASE IF NOT EXISTS `geo` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `geo`;


-- Dumping structure for table geo.bundles
DROP TABLE IF EXISTS `bundles`;
CREATE TABLE IF NOT EXISTS `bundles` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `datetime` datetime NOT NULL,
  `projectid` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_projectid` (`projectid`),
  CONSTRAINT `FK_projectid` FOREIGN KEY (`projectid`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table geo.bundles: ~1 rows (approximately)
/*!40000 ALTER TABLE `bundles` DISABLE KEYS */;
INSERT INTO `bundles` (`id`, `datetime`, `projectid`) VALUES
	(1, '2014-10-01 00:55:45', 1);
/*!40000 ALTER TABLE `bundles` ENABLE KEYS */;


-- Dumping structure for table geo.data
DROP TABLE IF EXISTS `data`;
CREATE TABLE IF NOT EXISTS `data` (
  `geo` varchar(50) NOT NULL,
  `paramid` bigint(20) unsigned NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY (`geo`,`paramid`),
  KEY `FK_paramid` (`paramid`),
  CONSTRAINT `FK_paramid` FOREIGN KEY (`paramid`) REFERENCES `parameters` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table geo.data: ~1 rows (approximately)
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
INSERT INTO `data` (`geo`, `paramid`, `value`) VALUES
	('100', 1, '5'),
	('100', 2, '45,5'),
	('100', 3, 'КИРПИЧНЫЙ'),
	('200', 1, '10'),
	('200', 2, '56,5'),
	('200', 3, 'ПАНЕЛЬНЫЙ'),
	('300', 1, '7'),
	('300', 2, '110,5'),
	('300', 3, 'ДЕРЕВЯННЫЙ');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;


-- Dumping structure for table geo.parameters
DROP TABLE IF EXISTS `parameters`;
CREATE TABLE IF NOT EXISTS `parameters` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bundleid` bigint(20) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bundleid` (`bundleid`),
  CONSTRAINT `FK_bundleid` FOREIGN KEY (`bundleid`) REFERENCES `bundles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table geo.parameters: ~3 rows (approximately)
/*!40000 ALTER TABLE `parameters` DISABLE KEYS */;
INSERT INTO `parameters` (`id`, `bundleid`, `name`, `type`) VALUES
	(1, 1, 'Этажность', 1),
	(2, 1, 'Площадь', 1),
	(3, 1, 'Тип', 2);
/*!40000 ALTER TABLE `parameters` ENABLE KEYS */;


-- Dumping structure for table geo.projects
DROP TABLE IF EXISTS `projects`;
CREATE TABLE IF NOT EXISTS `projects` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Dumping data for table geo.projects: ~1 rows (approximately)
/*!40000 ALTER TABLE `projects` DISABLE KEYS */;
INSERT INTO `projects` (`id`, `name`) VALUES
	(1, 'Жильё\r\n');
/*!40000 ALTER TABLE `projects` ENABLE KEYS */;
/*!40014 SET FOREIGN_KEY_CHECKS=1 */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
